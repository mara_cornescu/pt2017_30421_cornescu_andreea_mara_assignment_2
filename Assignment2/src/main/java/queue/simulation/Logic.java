package queue.simulation;

import java.util.ArrayList;
import java.util.List;

/**
 * The Logic class represents the most important part of this project. This is where the clients are
 * distributed to queues, here it is decided how each operation is done (enqueue and dequeue), and 
 * more importantly, in this class is implemented the run method that queue thread will execute.
 */

public class Logic {
	
	private int numberOfQueues;
	private int minServiceTime;
	private int maxServiceTime;
	private int simulationTime;
	private int minArrivalTime;
	private int maxArrivalTime;
	
	private int currentTime;
	private int numberOfClients;
	private int sumServiceTime;
	private int emptyQueueTime;
	private int peakHour;
	private int peakNumberOfClients;
	
	private List<Client> client;
	private Queue[] queues;
	private  List<String> watch;
	
	public Logic(int numberOfQueues, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime, int simulationTime) {
		
		this.numberOfQueues = numberOfQueues;
		this.minArrivalTime = minArrivalTime;
		this.maxArrivalTime = maxArrivalTime;
		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
		this.simulationTime = simulationTime;
		initAll();
		
	}
	
	private void initAll() {
		ClientGenerator allClients = new ClientGenerator(minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime, simulationTime);
		client = allClients.getAllClients();
		queues = new Queue[numberOfQueues];
		emptyQueueTime = 0;
		for(int i = 0; i<numberOfQueues; ++i) {
			queues[i] = new Queue();
		}
		watch = new ArrayList<String>();
	}
	
	private int increment() {	
		return currentTime++;
	}
	
	private int incrementNumberOfClients() {
		return numberOfClients++;
	}
	
	private int findQueue(Queue[] queues) {
		int minIndex = 0;
		
		for(int i = 0; i<numberOfQueues; ++i) {
			if(queues[i].getSize() < queues[minIndex].getSize()) {
				minIndex = i;
			}
		}
		return minIndex;
	}
	
	private boolean allQueuesFinished(Queue[] queues) {	
		boolean finished = true;
		
		for(int i = 0; i<numberOfQueues; ++i) {
			if(queues[i].getSize() != 0) 
				finished = false;
		}
		
		return finished;
	}
	
	private int peakNumberOfClients() {
		int max = 0;
		for(Queue q: queues) { 
			max += q.getSize();
		}
		return max;
	}

	
	private void dequeueLogic() {		
		for(int i = 0; i < numberOfQueues; ++i) {
			
			watch.add("Start queue number " + i + "\n" + queues[i] + "End queue number " + i + "\n");
		
			if(queues[i].getSize() == 0)
				emptyQueueTime++;
			
			if(queues[i].getSize() != 0)
				queues[i].getFirst().setServiceTime(queues[i].getFirst().getServiceTime() - 1);
		
			if(queues[i].getSize() != 0)
				if(queues[i].getFirst().getServiceTime() == 0) {
					queues[i].dequeue(queues[i].getFirst());
				}	
		}
	}
	
	private void enqueueLogic() {
		for(Client c : client) {
			if(currentTime == c.getArrivalTime()) {
				queues[findQueue(queues)].enqueue(c);
				sumServiceTime += c.getServiceTime();
				//System.out.println("CLientul" + c);
				if(queues[findQueue(queues)] != null)
					incrementNumberOfClients();
				break;
			}
		}
	}
	
	
	public void startSimulation() {
		Thread t = new Thread(new Runnable() {
			public void run() {
		
				while(currentTime < simulationTime || allQueuesFinished(queues) != true) {
					
					enqueueLogic();
					
				    if(peakNumberOfClients < peakNumberOfClients()) {
				    	peakNumberOfClients = peakNumberOfClients();
				    	peakHour = currentTime;
				    }
					
					dequeueLogic();
					
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
				
					increment();
				}
			}
		});
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
		}
	}
	
	
	public int getNumberOfClients() {
		return numberOfClients;
	}

	public int getWaitingTime() {
		int result = 0;
		for (Queue q: queues)
			result += q.getTotalWait();
		
		return result;
	}
	
	public int getServiceTime() {
		return sumServiceTime;
	}
	
	public int getEmptyQueueTime() {
		return emptyQueueTime;
	}
	
	public int getPeakNumberOfClients() {
		return peakNumberOfClients;	
	}
	
	public int getPeakHour() {
		return peakHour;
	}
	
	public String getAverageWaitingTime() {
		return String.format("%1.2f", (double) getWaitingTime()/getNumberOfClients());
	}
	
	public String getAverageServiceTime() {
		return String.format("%1.2f", (double) getServiceTime()/getNumberOfClients());
	}
	
	
	public String getWatch(int index) {
		String message = "";
		if(watch.isEmpty() == false) {
			message = watch.get(index);
		}
		return message;
	}
	
	public int getWatchSize() {
		return watch.size();
	}

}
