package queue.simulation;

import java.util.ArrayList;
import java.util.List;

/**
 * The Queue class implements the behavior of a queue using a list, more specifically an
 * arrayList of clients. A queue is able to add clients to the rear terminal position 
 * (enqueue), and to remove clients from the front terminal position (dequeue). The class 
 * also provides methods for getting the size of a queue, the first and the last element. 
 * For every queue is computed the sum of the waiting time and the service time of each 
 * client. Using the method toString each queue can be displayed on screen.
 */

public class Queue {
	
	private List<Client> clients;
	private int totalWait;
	
	public Queue(){
		this.clients = new ArrayList<Client>();
	}
	
	public void enqueue(Client c) {
		for(Client x: clients)
				totalWait += x.getServiceTime();
		clients.add(c);
	}
	
	public void dequeue(Client c) {
		if(clients.isEmpty() == false)
			clients.remove(0);
	}
	
	public void setTotalWait(int totalWait) {
		this.totalWait = totalWait;
	} 
	
	public int getTotalWait() {
		return totalWait;
	}
	
	public int getSize() {
		return clients.size();
	}
	
	public Client getFirst() {
		return clients.get(0);
	}
	
	public Client getLast() {
		return clients.get(clients.size() - 1);
	}
	
	public int sumServiceTime() {
		int sum = 0;
		
		for(Client c: clients)
			sum += c.getServiceTime();
		
		return sum;
	}
	 
	public String toString() {
		String queueClient = "";
		
		for(Client c: clients) {
			queueClient = queueClient + c.toString() + "\n";
		}
		
		return queueClient;
	}

}